(use regex)

(define-page (regexp "/static/.*")
             (lambda (path)
               (let ((filename (string-drop path 8)))
                 (send-static-file (string-append "static/" filename))))
             no-template: #t)

(add-request-handler-hook!
  'reload-on-request (lambda (path handler)
                       (reload-apps (awful-apps))
                       (handler)))
