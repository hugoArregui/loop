(use awful awful-blog srfi-1 traversal html-conversion-rules sxml-transforms lowdown)

(enable-sxml #t)

(page-css '("/static/css/bootstrap.min.css" "/static/css/site.css" "/static/css/bootstrap-theme.min.css"
            "http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css"))

(page-charset "utf-8")

(when (development-mode?)
  (include "src/development.scm"))

(define blog-url main-page-path)

(sxml->html
  (let ((rules `((literal *preorder* . ,(lambda (t b) b))
                 . ,(rules))))
    (lambda (sxml)
      (with-output-to-string
        (lambda ()
          (SRV:send-reply (pre-post-order* sxml rules)))))))

(define (wrap-content content)
  `((doctype-html)
    (html
      (body
        ((container
           ,content)
         (div (@ (class "container text-right"))
              (hr)
              (url "http://wiki.call-cc.org/eggref/4/awful" (img (@ (src "http://parenteses.org/mario/img/thats-awful.png")))))
         (js-link "/static/js/jquery-1.10.1.min.js")
         (js-link "/static/js/bootstrap.min.js"))))))

(define entry->bootstrap-markdown
  (let ((rules `((h1 *macro* . ,(lambda (t b)
                                  `(page-header (@ (class "h1")) ,@b)))
                 ,@alist-conv-rules*)))
    (lambda (entry)
      (let* ((file    (entry-resource entry))
             (sxml (with-input-from-file file (cut markdown->sxml (current-input-port)))))
        (pre-post-order* sxml rules)))))

(entry->sxml (lambda (entry)
               (wrap-content
                 (if (eq? (entry-type entry) 'markdown)
                   (entry->bootstrap-markdown entry)
                   (entry->sxml/default entry)))))

(define (link-tag current-tags tag)
  `(url ,(string-append "?tags=" current-tags "," (symbol->string tag)) ,tag))

(define (define-index-page url entries)
  (define-page url
    (lambda ()
      (let* (($tags          ($ 'tags ""))
             (selected-tags  (remove-duplicatesq (map string->symbol (string-split $tags ","))))
             (entries        (sort (filter-entries-by-tag entries selected-tags) string< entry-title))
             (filter-string  (if (null? selected-tags) "ALL" (string-join (map symbol->string selected-tags) "," )))
             (available-tags (sort
                               (filter (lambda (tag)
                                         (not (member tag selected-tags)))
                                       (remove-duplicatesq (concatenate (map entry-tags entries)))) string< symbol->string)))
        (set-page-title! (string-append "Index: " filter-string))
        (wrap-content `((page-header
                          (h3 "Index - " ,filter-string " -"))
                        (row
                          (div (@ (class "col-lg-3 tags"))
                               ,(if ((o not null?) available-tags)
                                  `((h3 "Tags:")
                                    (itemize ,@(map (cut link-tag $tags <>) available-tags)))
                                  '()))
                          (div (@ (class "col-lg-8 notes") (style "padding-left:20px"))
                               (h3 "Notes")
                               (itemize (@ (class "list-unstyled"))
                                        ,@(map (lambda (entry)
                                                 (let ((url  (index-url url entry))
                                                       (tags (filter (cut member <> available-tags) (entry-tags entry))))
                                                   `((url ,url ,(entry-title entry))
                                                     ,(if ((o not null?) tags)
                                                        (list " ( "
                                                              (map (lambda (tag)
                                                                     (list (link-tag $tags tag) " "))
                                                                   tags)
                                                              ")")
                                                        '()))))
                                               entries))))))))
    headers: '(meta (@ (name "viewport") (content "width=device-width, initial-scale=1.0")))
    title: "Index" ))

(define (define-blog-pages url)
  (let ((entries (eval (call-with-input-file "articles/index.scm" read))))
    (define-index-page url entries)
    (for-each (cut define-entry-page url <>) entries)))

(define-blog-pages (blog-url))
